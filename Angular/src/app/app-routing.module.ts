import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { MatchesComponent } from './matches/matches.component';
import { MatchAddComponent } from './matches/match-add.component';

const routes: Routes = [
  { path: '', redirectTo: '/matches', pathMatch: 'full' },
  { path: 'matches', component: MatchesComponent },
  { path: 'match-add', component: MatchAddComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
