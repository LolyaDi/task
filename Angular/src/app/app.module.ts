import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import {
  MatInputModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule
} from "@angular/material";

import { AppComponent } from './app.component';
import { MatchesComponent } from './matches/matches.component';
import { MatchAddComponent } from './matches/match-add.component';

@NgModule({
  declarations: [
    AppComponent,
    MatchesComponent,
    MatchAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
