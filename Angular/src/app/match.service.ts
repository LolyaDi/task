import { Injectable } from '@angular/core';

import { Observable} from 'rxjs';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Match } from './match';

@Injectable({ providedIn: 'root' })
export class MatchService {
  private url = 'https://localhost:5001/api/matches';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient : HttpClient) { }

  getMatches() : Observable<Match[]> 
  {
    return this.httpClient.get<Match[]>(this.url);  
  }
  
  addMatch(match : any) : Observable<Match>
  {
    return this.httpClient.post<Match>(this.url, match, this.httpOptions)
  }
}
