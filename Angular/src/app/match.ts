export class Match {
    date : Date;
    leagueName : string;
    firstTeamName : string;
    secondTeamName : string;
}
