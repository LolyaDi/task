import { Component, OnInit } from '@angular/core';

import {
  FormBuilder, 
  FormGroup, 
  NgForm,
  Validators
} from '@angular/forms';

import { Router } from '@angular/router';

import { MatchService } from '../match.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-match-add',
  templateUrl: './match-add.component.html',
  styleUrls: ['./match-add.component.css']
})
export class MatchAddComponent implements OnInit {
  matchForm: FormGroup;
  date: Date = null;
  leagueName: string = '';
  firstTeamName: string = '';
  secondTeamName: string = '';

  constructor(private router: Router, 
              private matchService: MatchService, 
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.matchForm = this.formBuilder.group({
      'date' : [null, Validators.required],
      'leagueName' : [null, Validators.required],
      'firstTeamName' : [null, Validators.required],
      'secondTeamName' : [null, Validators.required]
    });
  }

  onAdd(form: NgForm) {
    this.matchService.addMatch(form).subscribe(response => {
      this.router.navigate(['/matches']);
    },
    (error: HttpErrorResponse) => {
      console.error(error.status + ':' + error.error);
      alert('An error occured! match can\'t be added!');
    });
  }
}
