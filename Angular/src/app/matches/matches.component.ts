import { Component, OnInit } from '@angular/core';

import { Match } from '../match';
import { MatchService } from '../match.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  displayedColumns: string[] = ['date', 'leagueName', 'firstTeamName', 'secondTeamName'];
  dataSource: Match[] = [];

  constructor(private matchService : MatchService) { };

  ngOnInit() {
    this.getMatches();
  }

  getMatches() {
    this.matchService.getMatches().subscribe((matches) => {
      this.dataSource = matches;
    });
  }
}
