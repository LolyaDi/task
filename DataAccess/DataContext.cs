﻿using Microsoft.EntityFrameworkCore;
using System;

namespace DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<League> Leagues { get; set; }
        public DbSet<Teammate> Teammates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Match> Matches { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<League>()
                .HasIndex(league => league.Name)
                .IsUnique();

            modelBuilder.Entity<Team>()
                .HasIndex(team => team.Name)
                .IsUnique();

            modelBuilder.Entity<Teammate>()
                .HasIndex(teammate => new { teammate.Name, teammate.Surname })
                .IsUnique();

            modelBuilder.Entity<Match>()
                .HasOne(x => x.FirstTeam)
                .WithMany()
                .HasForeignKey(x => x.FirstTeamId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Match>()
                .HasOne(x => x.SecondTeam)
                .WithMany()
                .HasForeignKey(x => x.SecondTeamId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            var firstLeague = new League
            {
                Name = "League"
            };
            modelBuilder.Entity<League>().HasData(firstLeague);

            var firstTeam = new Team
            {
                Name = "FirstTeam"
            };
            var secondTeam = new Team
            {
                Name = "SecondTeam"
            };
            modelBuilder.Entity<Team>().HasData(firstTeam, secondTeam);

            modelBuilder.Entity<Match>()
                .HasData(new Match()
                {
                    Date = DateTime.Now,
                    LeagueId = firstLeague.Id,
                    FirstTeamId = firstTeam.Id,
                    SecondTeamId = secondTeam.Id
                }
           );
        }
    }
}
