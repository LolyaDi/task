﻿using System;

namespace DataAccess
{
    public class Match : Entity
    {
        public DateTime Date { get; set; }
        public Guid LeagueId { get; set; }
        public virtual League League { get; set; }
        public Guid FirstTeamId { get; set; }
        public virtual Team FirstTeam { get; set; }
        public Guid SecondTeamId { get; set; }
        public virtual Team SecondTeam { get; set; }
    }
}
