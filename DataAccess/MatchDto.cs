﻿using System;

namespace DataAccess
{
    public class MatchDto
    {
        public DateTime Date { get; set; }
        public string LeagueName { get; set; }
        public string FirstTeamName { get; set; }
        public string SecondTeamName { get; set; }
    }
}
