﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Leagues",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("d7c0c700-8721-4e0f-aa5a-c1131e63c550"), "League" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("997e3a32-a67d-4299-bb3f-5f8d29baa71d"), "FirstTeam" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("011444ac-53db-4d76-bb32-0e08501b5dfe"), "SecondTeam" });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "Date", "FirstTeamId", "LeagueId", "SecondTeamId" },
                values: new object[] { new Guid("d743d40d-fae4-4400-9a47-2e30524271a7"), new DateTime(2019, 9, 11, 0, 23, 17, 754, DateTimeKind.Local).AddTicks(7429), new Guid("997e3a32-a67d-4299-bb3f-5f8d29baa71d"), new Guid("d7c0c700-8721-4e0f-aa5a-c1131e63c550"), new Guid("011444ac-53db-4d76-bb32-0e08501b5dfe") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("d743d40d-fae4-4400-9a47-2e30524271a7"));

            migrationBuilder.DeleteData(
                table: "Leagues",
                keyColumn: "Id",
                keyValue: new Guid("d7c0c700-8721-4e0f-aa5a-c1131e63c550"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("011444ac-53db-4d76-bb32-0e08501b5dfe"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("997e3a32-a67d-4299-bb3f-5f8d29baa71d"));
        }
    }
}
