﻿namespace DataAccess
{
    public class Teammate : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}