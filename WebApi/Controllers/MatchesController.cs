﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataAccess;
using Mapster;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchesController : ControllerBase
    {
        private readonly DataContext _context;

        public MatchesController(DataContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public IEnumerable<MatchDto> GetMatchs()
        {
            return _context.Matches.Adapt<IEnumerable<MatchDto>>();
        }
        
        [HttpPost]
        public async Task AddMatch([FromBody]MatchDto matchDto)
        {
            var firstTeam = await _context.Teams.SingleOrDefaultAsync(x => x.Name == matchDto.FirstTeamName);
            var secondTeam = await _context.Teams.SingleOrDefaultAsync(x => x.Name == matchDto.SecondTeamName);
            var league = await _context.Leagues.SingleOrDefaultAsync(x => x.Name == matchDto.LeagueName);

            var match = new Match
            {
                FirstTeamId = firstTeam.Id,
                SecondTeamId = secondTeam.Id,
                LeagueId = league.Id,
                Date = matchDto.Date
            };

            _context.Matches.Add(match);
            await _context.SaveChangesAsync();
        }
    }
}
